  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
  import { getDatabase, ref, set, child, get, update, remove, onValue } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
  import { getAuth, signInWithEmailAndPassword, signOut, onAuthStateChanged } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyDUaQsWnpmApyhXMsfIjqJ4vXCOkWJkMzI",
    authDomain: "mysitio734.firebaseapp.com",
    databaseURL: "https://mysitio734-default-rtdb.firebaseio.com",
    projectId: "mysitio734",
    storageBucket: "mysitio734.appspot.com",
    messagingSenderId: "771547934655",
    appId: "1:771547934655:web:dd927a2ada0b5a189efb0a"
  };



  const app = initializeApp(firebaseConfig);
  const db = getDatabase();

  //----------------------------------------------------------
  var btnLoguear = document.getElementById('btnLoguear');
  var btnDisconnect = document.getElementById('btnDisconnect');
  var llenado = document.getElementById('llenar');
  var comprobar = document.getElementById('comprobarCuenta');
  var paginaFULL = document.getElementById('paginaADMIN');
  var btnlimpiar = document.getElementById('btnlimpiar')
  var email = "";
  var password = "";

  const auth = getAuth();

  function leer(){
    email = document.getElementById('correo').value;
    password = document.getElementById('pass').value;
    }

    function comprobarAUTH(){
      onAuthStateChanged(auth, (user) => {
        if (user) {
          alert("Usuario detectado");
          document.getElementById('todo').style.display="block";
          // ...
        } else {
          // User is signed out
          // ...
          alert("No se ha detectado un usuario");
          window.location.href="https://gated-spar.000webhostapp.com/html/Principal.html";
        }
      });
    }
  
    if(window.location.href == "https://gated-spar.000webhostapp.com/html/Administrador.html"){
      window.onload = comprobarAUTH();
    }
  
    
  if(btnLoguear){
    btnLoguear.addEventListener('click', (e)=>{
      leer();
      signInWithEmailAndPassword(auth, email, password)
        .then((userCredential) => {
          // Signed in 
          const user = userCredential.user;
          alert("¡INICIO DE SESIÓN EXITOSO!")
          window.location.href="https://gated-spar.000webhostapp.com/html/Administrador.html";
          // ...
        })
        .catch((error) => {
          alert("DATOS INCORRECTOS")
          const errorCode = error.code;
          const errorMessage = error.message;
        });
    });

  }
  if(btnDisconnect){
    btnDisconnect.addEventListener('click',  (e)=>{
      signOut(auth).then(() => {
        alert("¡CERRAR SESIÓN EXITOSO!")
        window.location.href="Principal.html";
        // Sign-out successful.
      }).catch((error) => {
        // An error happened.
      });
    });
  }
  function Rellamar(){
    if(btnDisconnect){
      btnDisconnect.addEventListener('click',  (e)=>{
        signOut(auth).then(() => {
          alert("¡CERRAR SESIÓN EXITOSO!")
          window.location.href="Principal.html";
          // Sign-out successful.
        }).catch((error) => {
          // An error happened.
        });
      });
    }

    if(comprobar){
      comprobar.addEventListener('click', (e)=>{
      const user = auth.currentUser;
      if (user !== null) {
        const email = user.email;
        alert(email);
    }
    });
  }

  }

  function limpiarbtn(){

  document.getElementById('correo').value = "";
  document.getElementById('pass').value = "";
  }

  btnlimpiar.addEventListener('click', limpiarbtn);
