  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
  import { getDatabase, onValue ,ref, set, child, get, update, remove} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
  import {  getStorage, ref as refS, uploadBytes, getDownloadURL, } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-storage.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyDUaQsWnpmApyhXMsfIjqJ4vXCOkWJkMzI",
    authDomain: "mysitio734.firebaseapp.com",
    databaseURL: "https://mysitio734-default-rtdb.firebaseio.com",
    projectId: "mysitio734",
    storageBucket: "mysitio734.appspot.com",
    messagingSenderId: "771547934655",
    appId: "1:771547934655:web:dd927a2ada0b5a189efb0a"
  };

  const app = initializeApp(firebaseConfig);
  const db = getDatabase();
  const storage = getStorage();
  
  // DeclaraciÃ³n de objetos
  var btnInsertar = document.getElementById('btnInsertar');
  var btnBuscar = document.getElementById('btnBuscar');
  var btnActualizar = document.getElementById('btnActualizar');
  var btnBorrar = document.getElementById('btnBorrar');
  var btnTodos = document.getElementById('btnTodos');
  var btnLimpiar = document.getElementById('btnLimpiar');
  var lista = document.getElementById('lista');
  var btnEliminar = document.getElementById('btnEliminar');
  var imgPreview = document.getElementById('Preview');
  var btnAgregar = document.getElementById('btnAgregar');
  var btnlimpiarcontactos = document.getElementById('btnlimpiarcon')
  var productos = document.getElementById('productos');
  var btnMostrarImagen = document.getElementById('verImagen');
  var archivo = document.getElementById('archivo');
  var ID = "";
  var nombre = "";
  var descripcion = "";
  var precio = "";
  var nombreIMG = "";
  var url = "";
  
  
  if(window.location.href == "https://gated-spar.000webhostapp.com/html/Productos.html"){
  
  }

  window.onload = mostrarProductos();
  function mostrarProductos(){
  
      const db = getDatabase();
      const dbRef = ref(db, 'cafeteria');
  
      onValue(dbRef, (snapshot) => {
          if(lista){
              lista.innerHTML = "";
          }
          snapshot.forEach((childSnapshot) => {
              const childKey = childSnapshot.key;
              const childData = childSnapshot.val();
  
              if(lista){
                if(childData.estado == 0){
                    lista.innerHTML = lista.innerHTML + "<div class='seccion3'> <br>" + "<img id='imagenespro' src=' " + childData.url + "'> <h2>" + childData.nombre + "</h2> <p>" + "ID: " + childKey + "<br>" +"<br>" + childData.descripcion + "</p><p> Precio: $" + childData.precio+ " MXN</p>" + "</div>";     
                }else if(childData.estado == 1){
                    lista.innerHTML = lista.innerHTML + "<div class='seccion3'> "+ "<img id='imagenespro' src=' " + childData.url + "'> <h2>" + childData.nombre + "</h2> <p>"  + childData.descripcion + "</p><p> Precio: $" + childData.precio+ " MXN</p>" + "<button id='botoncomprar'>COMPRAR</button></div>"; 
                    }   
              }else if(productos){
                if(childData.estado == 0){
                    productos.innerHTML = productos.innerHTML + "<div class='seccion3'> "+ "<img id='imagenesmostrar' src=' " + childData.url + "'> <h2>" +childData.nombre + "</h2> <p>"  + childData.descripcion + "</p><p> Precio: $" + childData.precio+ " MXN</p>" +"<br>"+ "<button id='botoncomprar'>COMPRAR</button> </div>";     
                }
              }
          
          });
      },{
          onlyOnce: true
      });
  
  }
  

  
  function leerInputs(){
      ID = document.getElementById('ID').value;
      nombre = document.getElementById('nombre').value;
      descripcion = document.getElementById('descripcion').value;
      precio = document.getElementById('precio').value;
      nombreIMG = document.getElementById('imgNombre').value;
      url = document.getElementById('url').value;
  }
  
  
async function insertarDatos(){

    await subirImagen();

    await leerInputs(); 

    await insertar();

    alert("Se ha insertado el producto");

    await limpiar();
}

async function insertar(){
    await set(ref(db,'cafeteria/' + ID), {
        nombre: nombre,
        descripcion: descripcion,
        precio: precio,
        nombreIMG: nombreIMG,
        url: url,
        estado: 0

    }).then((response)=>{
        alert("Se agregó con exito");
        mostrarProductos();
    }).catch((error)=>{
        alert("Surgio un error: " + error);
    });

    await limpiar();
}

// Esta función ELIMINAR/QUITA el producto de la base de datos
async function eliminar(){
    await leerInputs();

    if (ID == "") {
        alert("No hay ID que coincida con el producto");
        return;
    }else if(ID != null){
        remove(ref(db,'cafeteria/'+ ID)).then(()=>{
            alert("Se borró el registro");
            mostrarProductos();
        }).catch(()=>{
            alert("Surgió un error" + error );
        });
    }
}

// Sirve para deshabilitar el producto mediante el estado
async function borrar(){

    await leerInputs();

    if (ID == "") {
        alert("No hay ID que coincida con el producto");
        return;
    }else if(ID != null){
        await borrarEstado();
    }
    
}

async function borrarEstado(){

    update(ref(db,'cafeteria/'+ ID),{
        estado: 1
    }).then(()=>{
        alert("Se dio de baja correctamente");
        mostrarProductos();
    }).catch(()=>{
        alert("Ocurrio un error: " + error);
    });

}
    
async function agregar(){
    await leerInputs();

    if (ID == "") {
        alert("No hay ID que coincida con el producto");
        return;
    }else if(ID != null){
        await agregarEstado();
    }
}

async function agregarEstado(){
    update(ref(db,'cafeteria/'+ ID),{
        estado: 0
    }).then(()=>{
        alert("Se dio de alta correctamente");
        mostrarProductos();
    }).catch(()=>{
        alert("Ocurrio un error: " + error);
    });

    await limpiar();
}


  
  function mostrarDatos(){
      leerInputs();
      const dbref = ref(db);
      
      get(child(dbref,'cafeteria/' + ID)).then((snapshot)=>{
          if(snapshot.exists()) {
              nombre = snapshot.val().nombre;
              descripcion = snapshot.val().descripcion;
              precio = snapshot.val().precio;
              nombreIMG = snapshot.val().nombreIMG;
              url = snapshot.val().url;
  
              escribirInputs();
              document.getElementById('Preview').src = url;
          }else{
              alert("No existe el producto");
          }
      }).catch((error)=>{
          alert("Surgió un error: " + error);
      });
  }
  
  function escribirInputs(){
      document.getElementById('ID').value = ID;
      document.getElementById('nombre').value = nombre;
      document.getElementById('descripcion').value = descripcion;
      document.getElementById('precio').value = precio;
      document.getElementById('imgNombre').value = nombreIMG;
      document.getElementById('url').value = url;
  
  }
  
  async function actualizar(){

    await subirImagen();
    await leerInputs();
    await conseguir();

}

async function conseguir(){
    
        await update(ref(db,'cafeteria/'+ ID),{
            nombre:nombre,
            descripcion:descripcion,
            precio:precio,
            nombreIMG: nombreIMG,
            url: url
        }).then(()=>{
            alert("Se realizó la actualización");
            mostrarProductos();
        }).catch(()=>{
            alert("Surgió un error: " + error);
        });

        await limpiar();
    
}
  
  function limpiar(){
  
    lista.innerHTML="";
      ID = "";
      nombre = "";
      descripcion = "";
      precio = "";
      nombreIMG = "";
      url = "";
      imgPreview.src = "#";
      escribirInputs();
  
  }
  
  var file = "";
  var name = "";
  
  // Permite cargar la imagen
  function cargarImagen(){

    // archivo seleccionado
    file = event.target.files[0];
    name = event.target.files[0].name;
    document.getElementById('imgNombre').value = name;
    console.log(file);
    if(file){
        imgPreview.src = URL.createObjectURL(file);
    }
}
  
  // Llama la funciÃ³n cargar imagen y sube a la nube la imagen cargada
async function subirImagen(){
    // Sirve para subir la imagen al STORAGE
    const storageRef = refS(storage, 'Imagenes/' + name);

    // 'file' comes from the Blob or File API
    await uploadBytes(storageRef, file).then((snapshot) => {
        alert("Se cargo el archivo");
    });

    await descargarImagen();

}
  
 async function descargarImagen(){
  
      // import { getStorage, ref, getDownloadURL } from "firebase/storage";
      const storageRef = refS(storage, 'Imagenes/' + name);
  
      // Get the download URL
      await getDownloadURL(storageRef)
      .then((url) => {
          document.getElementById('url').value = url;
          document.getElementById('Preview').src = url;
      })
      .catch((error) => {
          // A full list of error codes is available at
          // https://firebase.google.com/docs/storage/web/handle-errors
          switch (error.code) {
          case 'storage/object-not-found':
              console.log("No existe el archivo")
              break;
          case 'storage/unauthorized':
              console.log("No tiene permisos");
              break;
          case 'storage/canceled':
              console.log("Se cancelo o no tiene internet");
              break;
  
          // ...
  
          case 'storage/unknown':
              console.log("Error desconocido");
              break;
          }
      });
  
  }
  
  function limpiarcontactos(){

    document.getElementById('nombrecontacto').value = "";

    document.getElementById('emailcontacto').value = "";

    document.getElementById('cajacomentario').value = "";
  }


  // Evento click
  if(btnInsertar){
      btnInsertar.addEventListener('click', insertarDatos);
  }
  
  if(btnBuscar){
      btnBuscar.addEventListener('click', mostrarDatos);
  }
  
  if(btnActualizar){
      btnActualizar.addEventListener('click', actualizar);
  }
  
  if(btnBorrar){
      btnBorrar.addEventListener('click', borrar);
  }
  
  if(btnTodos){
      btnTodos.addEventListener('click', mostrarProductos);
  }
  
  if(btnLimpiar){
      btnLimpiar.addEventListener('click', limpiar);
  }

  if(btnAgregar){
    btnAgregar.addEventListener('click', agregar);
}

  if(btnEliminar){
    btnEliminar.addEventListener('click', eliminar);
}

  if(archivo){
      archivo.addEventListener('change', cargarImagen);
  }
  
  
  if(btnMostrarImagen){
      btnMostrarImagen.addEventListener('click', descargarImagen)
  }

  if(btnlimpiarcontactos){
    btnlimpiarcontactos.addEventListener('click', limpiarcontactos)
  }



